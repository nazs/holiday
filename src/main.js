import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Routes from './routes'
import Vuetify from 'vuetify'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import { getData, postData, utils } from './func'

Vue.use(VueRouter)
Vue.use(Vuetify)


const router = new VueRouter({
  routes: Routes,
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  mode: 'history'
});

export const bus = new Vue({
  data() {
    return {
      debug: true,
      access: false,
      update: 1,
      url: '',
      users: [
        {username: 'staff1', name: 'Staff 1', password: '1111', type: 'user', quota: 14},
        {username: 'staff2', name: 'Staff 2', password: '1111', type: 'user', quota: 14},
        {username: 'admin', name: 'Admin System', password: '1111', type: 'super', quota: 14}
      ],
      usersOLD: [
        ['staff1', 'Staff 1', '1111', 'user', 14],
        ['staff2', 'Staff 2', '1111', 'user', 14],
        ['admin', 'Unit Admin', '1111', 'super', 14]
      ]
    }
  },
  created() {
    if(!this.getStore('users')) {
      this.setStore('users', this.users)
    }
  },
  methods: {
    updateQuota(i) {
      // for (let u of this.parseStore('users')) {
      //   if (u.username === i) {
      //     u.quota--
      //     bus.setStore('users', users)
      //     // this.bal = 'Remaining quota: [' + u.quota + ']'
      //     return 'Remaining quota: [' + u.quota + ']'
      //   }
      // }    
    },
    log(e, msg, debug=this.debug){
      utils.log(e, msg, debug)
    },
    getStore(i) {
      return utils.getStorage(i)
    },
    parseStore(i) {
      return utils.parseStorage(i)
    },
    setStore(k, v) {
      utils.setStorage(k, v)
    },
    removeStore(i) {
      utils.removeStore(i)
    },
    clearStore() {
      utils.clearStore()
    },
    get(url) {
      return getData(url)
    },
    post(url, params=null) {
      return postData(url, params)
    },
    formatDate(d) {
      return utils.formatDate(d)
    },
    now() {
      return utils.now()
    },
    quota(i) {
      // if(this.getStore('users')) {        
      // }
      // this.
    }
  }
})

new Vue({
  el: '#app',
  render: h => h(App),
  router
})
