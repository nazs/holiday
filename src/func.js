export const formatUpper = function(n) {
  // console.log('test now')
  return n.toUpperCase()

}

export const con = i => console.log(i)
export const cons = i => console.log(i)

export const utils = {
  parseStorage(i) {
    return JSON.parse( sessionStorage.getItem(i) )
  },
  getStorage(i) {
    return sessionStorage.getItem(i) 
  },
  setStorage(k, v) {
    sessionStorage.setItem( k, JSON.stringify(v) ) 
  },
  removeStore(i) {
    sessionStorage.removeItem(i)
  },
  clearStore() {
    sessionStorage.clear()
  },
  now() {
    var options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
    let now = new Date(Date.now()).toLocaleTimeString()
    let dateNow = new Date(Date.now()).toLocaleDateString('gb-GB', options)
    return dateNow + ' ' + now
  },
  log(e, msg, debug) {
    if (debug) {
      if (typeof (msg) == 'undefined') {
        console.log(e)
      } else {
        msg = msg + ':'
        console.log(msg + '\n', e)
      }
    }
  },
  formatDate(d) {
    const options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
    return new Date(d).toLocaleDateString('gb-GB', options)
  }
}

export const getData = (url) => {
  return new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url);
    xhr.onload = function () {
      if (this.status >= 200 && this.status < 300) {
        resolve(JSON.parse(xhr.response))
      } else { // url found but errors on page.
        reject(Error(`${url} failed to load; error code: ${xhr.statusText}`));
      }

    };
    xhr.onerror = function () { // url is not found
      reject(Error(`${url} failed to load; there was a network error.`));
    };
    xhr.send();
  });    
}

export const postData = (url, params) => {
  return new Promise(function (resolve, reject) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.onload = function () {
      if (this.status >= 200 && this.status < 300) {
        resolve(xhr.response)
        // resolve(JSON.parse(xhr.response))
      } else {
        reject(Error(`${url} failed to load; error code: ${xhr.statusText}`));
      }
    };
    xhr.onerror = function () {
      reject(Error(`${url} failed to load; there was a network error.`));
    };
    xhr.send(params);
  });
}

export const commonFunctions = {
  getAPI() {
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', url);
      xhr.onload = function () {
        if (this.status >= 200 && this.status < 300) {
          resolve(JSON.parse(xhr.response))
        } else { // url found but errors on page.
          reject(Error(`${url} failed to load; error code: ${xhr.statusText}`));
        }

      };
      xhr.onerror = function () { // url is not found
        reject(Error(`${url} failed to load; there was a network error.`));
      };
      xhr.send();
    });    
  }
  // postAPI
}