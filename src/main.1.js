import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Routes from './routes'
import Vuetify from 'vuetify'
import { store } from './store/store'
import axios from 'axios'
import Cart from './components/cart-class'
import Bill from './components/bill-class'
import { con, cons, getData, parseStorage, postData, stringStorage, setStorage } from './components/admin/func'

// export const axios
Vue.use(Vuetify)
Vue.use(VueRouter)

Vue.filter('lineBreak', function (value) {
  // return value.replace(/<br>/g, '\r\n');
  return value.replace(/(?:\r\n|\r|\n)/g, '<br />');
});

const router = new VueRouter({
  routes: Routes,
  scrollBehavior (to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  mode: 'history'
});


export const bus = new Vue({
  data() {
    return {
      counter: 0,
      userIn: false,
      menus: [],
      menuGroups: [],
      billSelected: false,
      cart: [],
      allCarts: [],
      allBills: [], // include attributes
      cartMode: false,
      starters: [],
      soups: [],
      noodles: [],
      vegetables: [],
      rice: [],
      url: 'http://172.16.1.6/tm470/',
      host: location.hostname,
      user: '',
      currentBill: '',
      amended: [],
      bills: [],
      billNos: [],
      menuCats: new Set(),
      debug: true,
      total: 0
    }
  },
  methods: {
    cons(i) {
      return cons(i)
    },
    get(url) {
      return getData(url)
    },
    post(url, params=null) {
      return postData(url, params)
    },
    now() {
      var options = { weekday: 'short', year: 'numeric', month: 'short', day: 'numeric' };
      let now = new Date(Date.now()).toLocaleTimeString()
      let dateNow = new Date(Date.now()).toLocaleDateString('gb-GB', options)
      return dateNow + ' ' + now 
    },   
    log(e, msg) {
      if(this.debug) {
        if(typeof(msg) == 'undefined') { 
          console.log(this.now() + '\n', e)
        } else {
          msg = msg+ ':'
          console.log(this.now(), msg + '\n', e)
        }
      }
    },
    pars(i) {
      return JSON.parse(i)
    },
    stri(i) {
      return JSON.stringify(i)
    },
    getStore(i) {
      return localStorage.getItem(i) 
    },
    setStore(k,v) {
      localStorage.setItem( k, JSON.stringify(v) )
    },
    myLog() {
      this.log('direct log');

    },
    getCount() {
      this.count++
      this.log(this.count)
    }
    , addFood(f) {
      this.food.push(f)

      // localStorage only stores in string type
      localStorage.setItem('food', JSON.stringify(this.food))

      let j = JSON.parse(localStorage.getItem('food'))
      this.log(j)
    }
    ,createBill(bill) {
      // this.log(bill)
      this.post(this.url, this.stri(bill))
        .then(data => {
          this.billSelected = data
          this.log(data)
          this.getBills()
        })
    }
    ,getBills() {
      this.get(this.url + '?showBills')
        .then(data => {
            // localStorage.setItem('allBills', JSON.stringify(data.data))
            this.allBills = data
            let b = []
            for(let i in this.allBills) {
              this.bills.push({id: this.allBills[i].id})
              this.billNos.push( JSON.parse(this.allBills[i].id) )
              b.push(new Bill(
                this.allBills[i].id, 
                this.allBills[i].table, 
                this.allBills[i].customer_id,
                this.allBills[i].pax
              ))
            }
            // this.log( JSON.stringify(this.billNos) )
            // this.log( JSON.stringify(this.allBills) )
            this.log(b, '141')
        }) 
    }
    ,getBillNos() {

    }
    ,getMenus() {
      this.get(this.url + '?getMenus')
      .then(data => {
        if(!localStorage.getItem('menus')) {
          localStorage.setItem('menus', JSON.stringify(data) )
        }
        data.map(menu => {
          this.menuCats.add(menu.category)
        })
        // this.log(this.menuCats)
        // this.log(this.menus)
      })
    }  
    ,getGroups() {
      this.get(this.url + '?getMenuGroups')
      .then(data => {
        this.menuGroups = data
        localStorage.setItem('groups', JSON.stringify(data))
        // this.log(this.menuGroups)
        return data
      })
    }
    ,setAPI() {
      this.url = location.hostname === '172.16.1.6' || location.hostname === 'localhost' ? 'http://172.16.1.6:81/tm470/' : 'https://nazs.net/'
    }
    ,setCurrent(c) {
      localStorage.setItem('current', c)
      this.log('current set')
      // alert(c + ' from main')
    }
    ,amendedCarts(c) {
      if(this.amended.indexOf(c) === -1) {
        this.amended.push(c)
        if(!localStorage.getItem('amended')) {
          localStorage.setItem('amended', this.amended )
          return
        }
        localStorage.setItem('amended', this.amended )
      }
    }
    ,updateTempCart(c) {
      setTimeout(() => {
        localStorage.setItem(this.billSelected + '-temp', JSON.stringify(c))        
      }, 100);
    }
    ,sum(c) {
      // alert(c)
      this.total = 0
      // setTimeout(() => {
        for (let i in c) {
          let sum = parseInt(c[i].qty) * parseFloat(c[i].price)
          this.total = this.total + sum
          // this.log(this.total)
          localStorage.setItem('total-' + this.billSelected, parseFloat(this.total).toFixed(2) ) 
        }
      // }, 0);      
    }
    ,add(id) {
      let cart = JSON.parse( localStorage.getItem(this.billSelected + '-temp') )
      this.log(cart, 'add item id ' + id)
      for(var i in cart) {
        if(cart[i].id === id) {
          cart[i].qty = parseInt(cart[i].qty)
          cart[i].qty = cart[i].qty + 1
          this.amendedCarts(JSON.parse(this.billSelected))
          this.updateTempCart(cart)
        }
      }
    }
    ,less(id) {
      let cart = JSON.parse( localStorage.getItem(this.billSelected + '-temp') )
      this.log(cart, 'find', id)
      for(var i in cart) {
        if(cart[i].id === id) {
          cart[i].qty = parseInt(cart[i].qty)
          if(cart[i].qty === 1) {
            cart.splice(i, 1);
            this.amendedCarts(JSON.parse(this.billSelected))
            this.updateTempCart(cart)
            return
          }
          cart[i].qty = cart[i].qty - 1
          this.amendedCarts(JSON.parse(this.billSelected))
          this.updateTempCart(cart)
        }
      }
    }
    ,addToCart(... args) {
      localStorage.setItem('cart', JSON.stringify(args))
      var basket = { "id": args[0], "bill_id": bus.billSelected, "item": args[1], "price": args[2], "qty": 1 };
  
      // let cart = bus.cart
      let cart = JSON.parse( localStorage.getItem(this.billSelected + '-temp') )
      // alert(typeof cart)
      this.log(cart)

      for (var i in cart) {
        if (cart[i].id === basket.id) {
          cart[i].qty = parseInt(cart[i].qty)
          cart[i].qty = cart[i].qty + 1
          localStorage.setItem('pushed', JSON.stringify(cart))
          this.updateTempCart(cart)
          this.amendedCarts(JSON.parse(this.billSelected))
          this.sum(cart)
          return;
        }
      }

        cart.push(basket)
        this.updateTempCart(cart)
        localStorage.setItem('pushed', JSON.stringify(cart))    
        this.amendedCarts(JSON.parse(this.billSelected))
        this.sum(cart)


          // this.log(cart)
      // } // check array length

      // localStorage.setItem('cart-Amended', bus.billSelected)


      // this.updateTempCart()
      // this.amendedCarts( JSON.parse(this.billSelected) )
      localStorage.setItem('cart-Amended', bus.billSelected)
    }    
  },

  created() {
    // con('test' ) 
    this.setAPI()
    this.getBills()
    this.getMenus()
    this.getGroups()
    // this.setCurrent()
    this.log('from main')

    var c1  = new Cart(2, 'item1', 'desc1', 2.2, 1)
    var c2  = new Cart(5, 'item2', 'desc2', 1.7, 1)
    var allc = []
    allc.push(c1)
    allc.push(c2)
    this.log(allc)
    let b1 = new Bill
    this.log(b1,'415')
    this.log('data', 'message')
    this.log(this.now, 'message 376')
    this.log(this.dateNow, 'message 377')
  }
});

new Vue({
  el: '#app',
  render: h => h(App),
  store,
  router
})
