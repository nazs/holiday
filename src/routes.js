import home from './components/home'
import login from './components/login'
import apply from './components/application'
import vuetify from './components/vuetify'

export default[
  { path: '/holiday', component: login},
  { path: '/apply', component: apply},
  { path: '/vuetify', component: vuetify},
  // { path: '/oneBill/:bill', component: oneBill},
  { path: '*', redirect: '/holiday'}

]